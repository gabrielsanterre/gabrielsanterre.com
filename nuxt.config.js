module.exports = {
    /*
     ** Headers of the page
     */
    head: {
        title: 'Développeur Web',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'Nuxt.js project'}
        ],
        link: [
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat:200,200i|Source+Code+Pro:900' },
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
            {rel: 'me', href: 'https://mastodon.fedi.100terres.com/@gab'}
        ]
    },

    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#E8A75D' },

    /*
     ** Customize the generated output folder
     */
    generate: {
        dir: 'public'
    },

    /*
     ** Customize the base url
     */
    router: {
        base: '/'
    },

    /*
     ** Build configuration
     */
    build: {
        /*
         ** Run ESLINT on save
         */
        extend (config, ctx) {
            if (ctx.isClient) {
                config.module.rules.push({
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /(node_modules)/
                })
            }
        }
    }
}
